﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NewsReport.Migrations
{
    public partial class NewsDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "NewsCategory",
                columns: table => new
                {
                    NewscategoryID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ShortName = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NewsCategory", x => x.NewscategoryID);
                });

            migrationBuilder.CreateTable(
                name: "newsList",
                columns: table => new
                {
                    NewsID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    NewscategoryID = table.Column<int>(nullable: false),
                    ReportDate = table.Column<string>(nullable: true),
                    NewsDetail = table.Column<string>(nullable: true),
                    GpsLat = table.Column<float>(nullable: false),
                    GpsLng = table.Column<float>(nullable: false),
                    NewsStatus = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_newsList", x => x.NewsID);
                    table.ForeignKey(
                        name: "FK_newsList_NewsCategory_NewscategoryID",
                        column: x => x.NewscategoryID,
                        principalTable: "NewsCategory",
                        principalColumn: "NewscategoryID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_newsList_NewscategoryID",
                table: "newsList",
                column: "NewscategoryID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "newsList");

            migrationBuilder.DropTable(
                name: "NewsCategory");
        }
    }
}
