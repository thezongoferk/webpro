#pragma checksum "D:\web pro\NewsReport\Pages\NewsAdmin\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "2f159ac5d0000695999fb3d102d599457cf1db01"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(NewsReport.Pages.NewsAdmin.Pages_NewsAdmin_Index), @"mvc.1.0.razor-page", @"/Pages/NewsAdmin/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.RazorPages.Infrastructure.RazorPageAttribute(@"/Pages/NewsAdmin/Index.cshtml", typeof(NewsReport.Pages.NewsAdmin.Pages_NewsAdmin_Index), null)]
namespace NewsReport.Pages.NewsAdmin
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\web pro\NewsReport\Pages\_ViewImports.cshtml"
using NewsReport;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2f159ac5d0000695999fb3d102d599457cf1db01", @"/Pages/NewsAdmin/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"88c01d45ccfb24904f468bbbfdaedb0a8afbac7d", @"/Pages/_ViewImports.cshtml")]
    public class Pages_NewsAdmin_Index : global::Microsoft.AspNetCore.Mvc.RazorPages.Page
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(26, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 4 "D:\web pro\NewsReport\Pages\NewsAdmin\Index.cshtml"
  
    ViewData["Title"] = "ระบบรายงานข่าว";

#line default
#line hidden
            BeginContext(78, 107, true);
            WriteLiteral("<div class=\"jumbotron\">\r\n    <center><h2>ระบบรายงานข่าวโดยประชาชนเพื่อประชาชน</h2></center>\r\n    </div>\r\n\r\n");
            EndContext();
#line 11 "D:\web pro\NewsReport\Pages\NewsAdmin\Index.cshtml"
 foreach (var item in Model.NewsCategory) {

#line default
#line hidden
            BeginContext(230, 43, true);
            WriteLiteral("        <div class=\"row\">\r\n            <h3>");
            EndContext();
            BeginContext(274, 38, false);
#line 13 "D:\web pro\NewsReport\Pages\NewsAdmin\Index.cshtml"
           Write(Html.DisplayFor(model=>item.ShortName));

#line default
#line hidden
            EndContext();
            BeginContext(312, 23, true);
            WriteLiteral("</h3>\r\n            <h4>");
            EndContext();
            BeginContext(336, 37, false);
#line 14 "D:\web pro\NewsReport\Pages\NewsAdmin\Index.cshtml"
           Write(Html.DisplayFor(model=>item.FullName));

#line default
#line hidden
            EndContext();
            BeginContext(373, 23, true);
            WriteLiteral("</h4>\r\n        </div>\r\n");
            EndContext();
#line 16 "D:\web pro\NewsReport\Pages\NewsAdmin\Index.cshtml"
}

#line default
#line hidden
            BeginContext(399, 7, true);
            WriteLiteral(" </div>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IndexModel> Html { get; private set; }
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<IndexModel> ViewData => (global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<IndexModel>)PageContext?.ViewData;
        public IndexModel Model => ViewData.Model;
    }
}
#pragma warning restore 1591
